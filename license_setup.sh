#!/bin/bash

# Edit redsocks.conf
sudo cp /local/repository/redsocks.conf /etc/

# This is a temporary fix until I get the dataset fixed to have the right
# default of 27030@winlic-a.eng.utah.edu instead of winlic-a.eng.utah.edu:27030
mkdir -p ~/.config/CST\ AG/
cp /local/repository/CST\ DESIGN\ ENVIRONMENT.conf ~/.config/CST\ AG/
touch ~/.config/CST\ AG/2021-settings-updated

echo Establish an ssh connection to a computer with access to the CADE license
echo servers with the -D 8080 option to setup the SOCKS5 proxy for license
echo server access. Ping something \(like 8.8.8.8\) to keep the connection
echo alive. Press ctrl+A, D once this is done.
echo
echo Press enter to get the prompt.

read

screen -S socks_proxy bash

sudo systemctl restart redsocks
sudo iptables -t nat -A OUTPUT -d 155.98.110.80/32 -p tcp -j REDIRECT --to-ports 12345

# winlic-a.eng.utah.edu:27030
# user: POWDER

# Do something to save the iptables for next time? Or does this need to ssh,
# restart redsocks, redo iptables each time?

echo All done
