import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab.pnext as pn
import geni.rspec.igext as ig
import geni.rspec.emulab.spectrum as spectrum


def make_profile(disk_image, read_only=True):
    portal.context.defineParameter("node_id",
                                   "ID of the node to get",
                                   portal.ParameterType.STRING, "")

    params = portal.context.bindParameters()
    request = portal.context.makeRequestRSpec()

    node = request.RawPC("node")
    node.component_id = params.node_id
    node.disk_image = disk_image

    iface = node.addInterface()
    fsnode = request.RemoteBlockstore("fsnode", "/opt/cst")
    fsnode.dataset = "urn:publicid:IDN+emulab.net:powderteam+ltdataset+CST-install"
    fsnode.readonly = read_only

    fslink = request.Link("fslink")
    fslink.addInterface(iface)
    fslink.addInterface(fsnode.interface)

    fslink.best_effort = True
    fslink.vlan_tagging = True

    portal.context.printRequestRSpec()
